resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_name}"
  acl = "${var.acl}"
  force_destroy = "${var.force_destroy}"

  versioning {
    enabled = "${var.versioning_enabled}"
  }

  website {
    index_document = "${var.index_document}"
    error_document = "${var.error_document}"
    routing_rules = "${var.routing_rules}"
  }

  tags {
    Name = "${var.bucket_name}"
  }
}
