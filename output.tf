output "id" {
  value = "${aws_s3_bucket.bucket.id}"
}

output "arn" {
  value = "${aws_s3_bucket.bucket.arn}"
}

output "hosted_zone_id" {
  value = "${aws_s3_bucket.bucket.hosted_zone_id}"
}

output "region" {
  value = "${aws_s3_bucket.bucket.region}"
}

output "website_endpoint" {
  value = "${aws_s3_bucket.bucket.website_endpoint}"
}

output "website_domain" {
  value = "${aws_s3_bucket.bucket.website_domain}"
}
