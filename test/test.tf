/* Unit test file for module
 * =========================
 * Declare the folowing variables in environment to execute this code:
 * - AWS_ACCESS_KEY_ID
 * - AWS_SECRET_ACCESS_KEY
 * - AWS_DEFAULT_REGION
 * - AWS_SECURITY_TOKEN (if applicable)
 */

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
variable account {}

// Call the module
module "test_s3" {
  source = "../"

  account = "${var.account}"
  bucket_name = "static-website-acme-test"
  index_document = "root.html"
  error_document = "error.html"
  force_destroy = "true"
  routing_rules = "${file("config/routing_rules.json")}"
}

resource "aws_s3_bucket_object" "root" {
  key = "root.html"
  bucket = "${module.test_s3.id}"
  source = "site/root.html"
  acl = "public-read"
  content_type = "text/html"
}

resource "aws_s3_bucket_object" "error" {
  key = "error.html"
  bucket = "${module.test_s3.id}"
  source = "site/error.html"
  acl = "public-read"
  content_type = "text/html"
}
